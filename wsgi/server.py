# coding=utf-8
import json
import os
import re
import sys
import time
from multiprocessing import Process

from flask import Flask, logging, request, render_template, session, url_for, g, flash
from flask.json import jsonify
from flask_login import LoginManager, login_required
from werkzeug.utils import redirect

from rr_lib.cm import ConfigManager
from rr_lib.users.views import users_app, usersHandler
from states.processes import ProcessDirector
from wsgi import tst_to_dt, array_to_string
from wsgi.db import HumanStorage
from wsgi.properties import YT_RESULTS_COUNT
from wsgi.rr_people import S_WORK, S_SUSPEND, S_STOP, S_END
from wsgi.rr_people.posting import POST_GENERATOR_OBJECTS
from wsgi.rr_people.posting.copy_gen import SubredditsRelationsStore
from wsgi.rr_people.posting.posts import PS_BAD, PostsStorage, PS_PREPARED
from wsgi.rr_people.posting.posts_generator import PostsGenerator
from wsgi.rr_people.posting.posts_important import IMPORTANT_POSTS_SUPPLIER_PROCESS_ASPECT
from wsgi.rr_people.posting.posts_important import ImportantYoutubePostSupplier
from wsgi.youtube import get_flow
from wsgi.youtube.engine import YoutubeManager
from wsgi.youtube.infrastructure import get_data_from_input_or_default, from_dict_to_list, YoutubeFilterDataManager, \
    aggregate_videos_datas
from wsgi.youtube.sniffer import SnifferStorage, Sniffer, ASPECT_SNIFFER, sn_log
from wsgi.youtube.sniffer import get_auth_service
from wsgi.youtube.sniffer.auth import check_yt
from wsgi.youtube.sniffer.views import SnifferChannelsApi, SnifferVideosApi, SnifferManageApi, SnifferCategoryApi, \
    SnifferCheckCategoryApi
from wsgi.youtube.store import YMStorage

__author__ = '4ikist'

reload(sys)
sys.setdefaultencoding('utf-8')

log = logging.getLogger("web")

cur_dir = os.path.dirname(__file__)
config = ConfigManager(group=1)
app = Flask("Humans", template_folder=cur_dir + "/templates", static_folder=cur_dir + "/static")

app.secret_key = config.get("app_secret_key")
app.config['SESSION_TYPE'] = 'filesystem'

app.register_blueprint(users_app, url_prefix="/u")

app.jinja_env.filters["tst_to_dt"] = tst_to_dt
app.jinja_env.globals.update(array_to_string=array_to_string)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'


@app.before_request
def load_user():
    if session.get("user_id"):
        user = usersHandler.get_by_id(session.get("user_id"))
    else:
        user = usersHandler.get_guest()
    g.user = user


@login_manager.user_loader
def load_user(userid):
    return usersHandler.get_by_id(userid)


@login_manager.unauthorized_handler
def unauthorized_callback():
    return redirect(url_for('users_api.login'))


@app.route("/")
@login_required
def main():
    user = g.user
    return render_template("main.html", **{"username": user.name})


splitter = re.compile('[^\w\d_-]*')

db = HumanStorage(name="hs server")
srs = SubredditsRelationsStore("server")
posts_storage = PostsStorage("server", hs=db)
posts_generator = PostsGenerator()


@app.route("/posts")
@login_required
def posts():
    subs = db.get_subs_of_all_humans()
    qp_s = {}
    subs_states = {}
    for sub in subs:
        qp_s[sub] = posts_storage.get_posts_for_sub_with_state(sub, state=PS_PREPARED)
        subs_states[sub] = posts_generator.states_handler.get_posts_generator_state(sub) or S_STOP

    human_names = map(lambda x: x.get("user"), db.get_humans_info(projection={"user": True}))

    stat = posts_storage.get_all_posts_states()

    return render_template("posts.html", **{"subs": subs_states, "qp_s": qp_s, "humans": human_names, "stat": stat})


@app.route("/posts/states/<state>")
@login_required
def posts_with_state(state):
    posts = posts_storage.get_posts_with_state(state)
    states = posts_storage.get_all_posts_states()
    return render_template("posts_state.html", **{"posts": posts, "state": state, "states": states})


@app.route("/posts/change_state", methods=["POST"])
@login_required
def change_post_state():
    data = json.loads(request.data)
    url_hash = data.get("url_hash")
    new_state = data.get("new_state")
    if new_state and url_hash:
        result = posts_storage.set_post_state(url_hash, new_state)
        if result.modified_count > 0:
            return jsonify(**{"ok": True})
    return jsonify(**{"ok": False})


@app.route("/generators", methods=["GET", "POST"])
@login_required
def gens_manage():
    if request.method == "POST":
        sub = request.form.get("sub")
        generators = request.form.getlist("gens[]")
        related_subs = request.form.get("related-subs")
        key_words = request.form.get("key-words")

        related_subs = splitter.split(related_subs)
        key_words = splitter.split(key_words)

        srs.add_sub_relations(sub, related_subs)
        posts_generator.generators_storage.set_sub_gen_info(sub, generators, key_words)

        flash(u"Генераторъ постановленъ!")
    gens = POST_GENERATOR_OBJECTS.keys()
    subs = db.get_subs_of_all_humans()
    return render_template("generators.html", **{"subs": subs, "gens": gens})


@app.route("/generators/sub_info", methods=["POST"])
@login_required
def sub_gens_cfg():
    data = json.loads(request.data)
    sub = data.get("sub")
    related = srs.get_related_subs(sub)
    generators = posts_generator.generators_storage.get_sub_gen_info(sub)

    return jsonify(**{"ok": True, "related_subs": related, "key_words": generators.get("key_words"),
                      "generators": generators.get("gens")})


@app.route("/generators/start", methods=["POST"])
@login_required
def sub_gens_start():
    data = json.loads(request.data)
    sub = data.get("sub")
    if sub:
        posts_generator.states_handler.set_posts_generator_state(sub, S_WORK)
        posts_generator.start_generate_posts(sub)
        return jsonify(**{"ok": True, "state": S_WORK})
    return jsonify(**{"ok": False, "error": "sub is not exists"})


@app.route("/generators/pause", methods=["POST"])
@login_required
def sub_gens_pause():
    data = json.loads(request.data)
    sub = data.get("sub")
    if sub:
        posts_generator.states_handler.set_posts_generator_state(sub, S_SUSPEND, ex=3600 * 24 * 7)
        return jsonify(**{"ok": True, "state": S_SUSPEND})
    return jsonify(**{"ok": False, "error": "sub is not exists"})


@app.route("/generators/del_post", methods=["POST"])
@login_required
def del_post():
    data = json.loads(request.data)
    p_hash = data.get("url_hash")
    if p_hash:
        posts_storage.set_post_state(p_hash, PS_BAD)
        return jsonify(**{"ok": True})
    return jsonify(**{"ok": False, "error": "post url hash is not exists"})


@app.route("/generators/del_sub", methods=["POST"])
@login_required
def del_sub():
    data = json.loads(request.data)
    sub_name = data.get("sub_name")
    if sub_name:
        posts_generator.terminate_generate_posts(sub_name)
        db.remove_sub_for_humans(sub_name)
        posts_storage.remove_posts_of_sub(sub_name)
        posts_generator.states_handler.remove_post_generator(sub_name)
        return jsonify(**{"ok": True})

    return jsonify(**{"ok": False, "error": "sub is not exists"})


@app.route("/generators/prepare_for_posting", methods=["POST"])
@login_required
def prepare_for_posting():
    data = json.loads(request.data)
    sub = data.get("sub")
    if sub:
        posts_storage.move_posts_to_ready_state(sub)
        return jsonify(**{"ok": True})

    return jsonify(**{"ok": False, "error": "sub is not exists"})


@app.route("/generators/start_all", methods=["POSt"])
@login_required
def start_all():
    def f():
        subs = db.get_subs_of_all_humans()
        pg = PostsGenerator()
        for sub in subs:
            pg.states_handler.set_posts_generator_state(sub, S_WORK)
            pg.start_generate_posts(sub)
            while 1:
                if posts_generator.states_handler.get_posts_generator_state(sub) == S_END:
                    break
                else:
                    time.sleep(1)

    p = Process(target=f)
    p.start()

    return jsonify(**{"ok": True})


@app.route("/queue/posts/<name>", methods=["GET"])
@login_required
def queue_of_posts(name):
    queue = posts_storage.get_ready_posts(name=name)
    return render_template("posts_ready.html", **{"human_name": name, "queue": queue})


ym_store = YMStorage("server")
ym = YoutubeManager()
yfdm = YoutubeFilterDataManager()

@app.route("/youtube", methods=["GET", "POST"])
@login_required
def youtube_manage():
    if request.method == "GET":
        default = get_data_from_input_or_default({})
        exps_info_results = ym_store.get_all_experiments_data()
        return render_template("youtube_manage.html", exp_data=default, load_exps_results=exps_info_results)

    if request.method == "POST":
        filter_data = get_data_from_input_or_default(request.form)

        channels = {}
        counter = 0
        for channel_id, channel_data in yfdm.get_ids_of_channels(filter_data):
            channels[channel_id] = channel_data
            counter += 1
            if counter == YT_RESULTS_COUNT:
                break

        if channels:
            exp_id, filter_data = ym_store.new_experiment(**filter_data)
            for channel_id, channel_data in channels.iteritems():
                ym_store.update_channel(channel_id, channel_data, exp_id)
            return redirect(url_for("youtube_exp_info", exp_id=exp_id))
        else:
            flash(u"Нихуя не найденно:(")
            exps_info_results = ym_store.get_all_experiments_data()
            return render_template("youtube_manage.html", exp_data=filter_data, load_exps_results=exps_info_results)


@app.route("/youtube/exp_info/<exp_id>")
@login_required
def youtube_exp_info(exp_id):
    exp_data = ym_store.get_experiment_data(exp_id)

    channels = []
    if exp_data.get("channels"):
        channels.extend(list(ym_store.get_channels_info(exp_data.get("channels"))))
    return render_template("youtube_search_info.html", **{"exp_data": exp_data, "channels": channels})


@app.route("/youtube/load_channel/<channel_id>/<exp_id>")
@login_required
def load_channel(channel_id, exp_id):
    try:
        exp_data = ym_store.get_experiment_data(exp_id)
        if exp_data:
            tags, words = aggregate_videos_datas(yfdm.get_channel_videos(channel_id, exp_data))
            ch_info = ym_store.get_channel_info(channel_id)
            if tags and words:
                return jsonify(**{"ok": True, "tags": from_dict_to_list(tags), "words": from_dict_to_list(words),
                                  "channel_name": ch_info.get("title")})
            else:
                return jsonify(**{"ok": False, "error": "Нихуя не найденно для канала: %s" % ch_info.get("title")})
        else:
            return jsonify(**{"ok": False, "error": "%s not found in experiments" % exp_id})
    except Exception as e:
        return jsonify(**{"ok": False, "error": str(e)})


@app.route("/youtube/delete_exp/<exp_id>", methods=["POST"])
@login_required
def delete_exp(exp_id):
    ym_store.delete_exp(exp_id)
    return jsonify(**{"ok": True})


@app.route('/youtube/callback')
def youtube_callback():
    log.info("YTcb args:%s" % (request.args))
    flow = get_flow()
    if 'code' not in request.args:
        auth_uri = flow.step1_get_authorize_url()
        log.info("YTcb STEP1\nwill redirect to: %s" % auth_uri)
        return redirect(auth_uri)
    else:
        auth_code = request.args.get('code')
        credentials = flow.step2_exchange(auth_code)
        credentials_data = json.loads(credentials.to_json())
        channel_id = sn_store.get_current_channel_id()
        if not channel_id:
            log.error("Not client id on yt/callback it is very bad!")
            return redirect(url_for('sniffer_manage', error="Channel id is not set!"))

        log.info("YTcb STEP2\nclient_id:%s\ncode:\t%s\ncreds:\t%s" % (channel_id, auth_code, credentials_data))
        ym_store.set_oauth_credentials(channel_id, credentials_data)

        return render_template('sn_manage.html', credentials_data=credentials_data)


sn_store = SnifferStorage("server")
pd = ProcessDirector("server")


@app.route('/youtube/check')
def youtube_check():
    channel_id = sn_store.get_current_channel_id()
    if not channel_id:
        return jsonify(**{"error": "current channel id is not known"})

    yt, channel = get_auth_service(ym_store, channel_id)
    if not yt:
        return jsonify(**{"error": "yt is can not init"})

    result = check_yt(yt, channel_id, sn_log)
    return jsonify(**{"current_channel": channel_id, "channel_data": channel, "check": result})


app.add_url_rule('/sniffer/channels', view_func=SnifferChannelsApi.as_view("sniffer_channels"))

sniffer_videos_view = SnifferVideosApi.as_view("sniffer_videos")
app.add_url_rule('/sniffer/video', view_func=sniffer_videos_view)
app.add_url_rule('/sniffer/video/<video_id>', view_func=sniffer_videos_view, methods=['DELETE'])
app.add_url_rule('/sniffer/video/add', view_func=sniffer_videos_view, methods=['PUT'])

sniffer_categories = SnifferCategoryApi.as_view("sniffer_categories")
app.add_url_rule('/sniffer/categories', view_func=sniffer_categories, methods=['GET', 'POST'])
app.add_url_rule('/sniffer/categories/<category_name>', view_func=sniffer_categories, methods=['DELETE'])
app.add_url_rule('/sniffer/config', view_func=sniffer_categories, methods=['PUT'])

sniffer_cat_check = SnifferCheckCategoryApi.as_view("sniffer_cat_check")
app.add_url_rule("/sniffer/cat_check", view_func=sniffer_cat_check, methods=['POST'])

app.add_url_rule('/sniffer/manage', view_func=SnifferManageApi.as_view("sniffer_manage"))


def _check_module(module_name):
    config = ConfigManager(group=100)
    value = config.get(module_name)
    if value:
        if isinstance(value, dict):
            return value.get("on")
        value = value.lower()
        if value != 'false':
            return True
    return False


if _check_module('im_po_su'):
    im_po_su = ImportantYoutubePostSupplier()
    if not pd.is_aspect_work(IMPORTANT_POSTS_SUPPLIER_PROCESS_ASPECT, timing_check=False):
        im_po_su.start()


    @app.route("/load_important", methods=["POST"])
    def load_important():
        data = json.loads(request.data)
        if "key" in data:
            count_loaded, e = im_po_su.load_new_posts_for_human(data.get("name"), data.get("channel_id"))
            if e:
                return jsonify(**{"ok": False, "error": e})
            return jsonify(**{"ok": True, "key": data.get("key"), "loaded": count_loaded})
        return jsonify(**{"ok": False, "fuck off": u"вы кто такие я вас не звал идите нахуй"})
else:
    log.info("[im po su] is NOT started. Why not!:)")

if _check_module('wake_up'):
    from wake_up.views import wake_up_app

    app.register_blueprint(wake_up_app, url_prefix="/wake_up")
    app.jinja_env.globals.update(wake_up=True)
else:
    log.info("[wake_up] is NOT started. Because you want this/")

if _check_module('sniffer'):

    def start_sniffer(ch_store, ym_store):
        from wsgi.youtube.sniffer import sn_log
        channel_id = ch_store.get_current_channel_id()

        yt, channel = get_auth_service(ym_store, channel_id)
        if not yt or not channel:
            sn_log.error("Yt engine is not present or can not start...")
            return

        sn_log.info("Auth channel: \n%s" % (channel))
        sniffer = Sniffer(yt)
        sn_log.info("[sniffer] start!")
        sniffer.start()
        return sniffer


    log.info("Will start [sniffer]")
    if not pd.is_aspect_work(ASPECT_SNIFFER, timing_check=False):
        start_sniffer(sn_store, ym_store)
        log.info('[sniffer] started')
else:
    log.info("[sniffer] is NOT started. As you wish...")

if __name__ == '__main__':
    port = 65010
    while 1:
        print port
        try:
            app.run(port=port)
        except:
            port += 1
