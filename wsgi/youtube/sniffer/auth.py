from wsgi.youtube.engine import YoutubeManager
from wsgi.youtube.sniffer import update_video
from wsgi.youtube.sniffer.store import SnifferStorage
from wsgi.youtube.store import YMStorage

yt_store = YMStorage('check')
sn_store = SnifferStorage('check')


def check_yt(yt, channel_id, log):
    ym = YoutubeManager(yt=yt)
    channel_id, videos = ym.get_channel_info(channel_id)
    for video in videos:
        try:
            result = update_video(yt, video.get('video_id'), video.get('title') + "+")
            if 'error' in result:
                return result

            result = update_video(yt, video.get('video_id'), video.get('title'))
            if 'error' in result:
                return result

        except Exception as e:
            log.exception(e)
            return False

        return True
