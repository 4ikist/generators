# coding=utf-8
import json
import logging

from flask import flash
from flask import request
from flask import url_for
from flask.json import jsonify
from flask.templating import render_template
from flask.views import MethodView
from werkzeug.utils import redirect

from wsgi.youtube.engine import YoutubeManager
from wsgi.youtube.sniffer import CategoriesRetriever, get_log_data, process_director, ASPECT_SNIFFER, sn_log, reset_log
from wsgi.youtube.sniffer import Sniffer
from wsgi.youtube.sniffer import get_auth_service
from wsgi.youtube.sniffer.store import SnifferStorage, SnifferConfigStorage, VT_TRACKED
from wsgi.youtube.store import YMStorage

sn_store = SnifferStorage("sniffer_api")
cfg_store = SnifferConfigStorage("sniffer_api")
cat_retriever = CategoriesRetriever(cfg_store)

ym_store = YMStorage("sniffer_api")
ym = YoutubeManager()

log = logging.getLogger("sniffer_api")
profile = 'default'


class SnifferManageApi(MethodView):
    methods = ["POST", "GET", "PATCH"]

    def get(self):
        result_params = {}

        current_channel_id = sn_store.get_current_channel_id()
        if current_channel_id:
            current_channel = self._get_channel_data(current_channel_id)
            result_params['current_channel'] = current_channel
            result_params['current_credentials'] = ym_store.get_oauth_credentials(current_channel_id)

        result_params['state'] = process_director.is_aspect_work(ASPECT_SNIFFER)
        result_params['log'] = get_log_data()

        return render_template("sn_manage.html", **result_params)

    def post(self):
        channel_id = request.form.get("channel_id")
        sn_store.set_current_channel_id(channel_id)
        log.info("Added channel_id: %s" % channel_id)

        yt, mine_channel = get_auth_service(ym_store, channel_id)
        if not yt and not mine_channel:
            return redirect(url_for('youtube_callback'))

        if mine_channel.get('channel_id') != channel_id:
            return render_template("sn_manage.html",
                                   channel_set=channel_id,
                                   channel_login=mine_channel.get('channel_id'))

        sn_store.store_channel(mine_channel.get('channel_id'), mine_channel)

        log.info("Starting sniffer! For channel: [%s]" % channel_id)
        sniffer = Sniffer(yt)
        sniffer.start()

        current_channel = self._get_channel_data(channel_id)
        return render_template("sn_manage.html",
                               current_channel=current_channel,
                               current_credentials=ym_store.get_oauth_credentials(channel_id),
                               state=True)

    def patch(self):
        data = json.loads(request.data)
        if data['action'] == 'stop':
            Sniffer.must_stop()

        if data['action'] == 'reset_log':
            reset_log()

        if data['action'] == 'reset_credentials':
            current_channel_id = sn_store.get_current_channel_id()
            if current_channel_id:
                ym_store.oauth_data.delete_one({'channel_id': current_channel_id})

        if data['action'] == 'start':
            current_channel_id = sn_store.get_current_channel_id()
            yt, mine_channel = get_auth_service(ym_store, current_channel_id)
            if yt:
                sn = Sniffer(yt)
                sn.start()


        return jsonify(**{'ok': True})

    def _get_channel_data(self, channel_id):
        channel = sn_store.get_channel(channel_id)
        if not channel:
            channel, _ = ym.get_channel_info(channel_id, add_videos=False)
            if channel:
                sn_store.store_channel(channel_id, channel)
        return channel


class SnifferChannelsApi(MethodView):
    def get(self):
        channels_sniffed = sn_store.get_channels_sniffed()

        current_channel_id = sn_store.get_current_channel_id()
        current_channel, _ = ym.get_channel_info(current_channel_id, add_videos=False)

        return render_template("sn_channels.html", **{"channels": channels_sniffed,
                                                      "current_channel": current_channel,
                                                      })

    def post(self):
        data = json.loads(request.data)
        channel_id = data.get('channel_id')
        if not channel_id:
            return jsonify(**{"ok": False, "error": "No channel id"})
        channel, _ = ym.get_channel_info(channel_id, add_videos=False)
        if not channel:
            return jsonify(**{"ok": False, "error": "Can not load channel info from YT :("})
        channel['sniffed'] = True
        sn_store.store_channel(channel_id, channel)
        return jsonify(**{"ok": True})

    def delete(self):
        data = json.loads(request.data)
        channel_id = data.get('channel_id')
        result = sn_store.delete_channel(channel_id)
        if result.deleted_count != 0:
            return jsonify(**{"ok": True})
        return jsonify(**{"ok": False, "error": "No channels deleted"})


def _filter_for_channel(for_channel):
    return filter(lambda x: x and len(x) == 24, map(lambda x: x.strip(), for_channel))


class SnifferVideosApi(MethodView):
    methods = ['POST', 'GET', 'DELETE', 'PUT']

    def post(self):
        data = json.loads(request.data)
        video_id, categories, for_channel = data.get('video_id'), data.get('categories'), _filter_for_channel(
            data.get('for_channel'))
        sn_log.info("Update [%s] video for publishing! \nWith category: [%s]\nFor channel: [%s]",
                    (video_id, categories, for_channel))

        if video_id and categories:
            video_data = ym.get_video_info(video_id)
            video_data['categories'] = categories
            if for_channel:
                video_data['for_channel'] = for_channel

            sn_store.store_video(video_id, video_data, VT_TRACKED)

            return jsonify(**{"ok": True})

        sn_log.info("Something wrong with this video: [%s]" % video_id)
        return jsonify(**{"ok": False, "error": "required field is not present :("})

    def delete(self, video_id):
        result = sn_store.delete_video(video_id)
        if result.deleted_count > 0:
            return jsonify(**{"ok": True})
        else:
            return jsonify(**{"ok": False, "error": "not found :("})

    def get(self):
        tracked_videos = sn_store.get_videos_with_type(VT_TRACKED)
        categories = map(lambda x: x.get('name'), cfg_store.get_all_categories())
        return render_template("sn_videos.html", videos=tracked_videos, categories=categories)

    def put(self):
        data = json.loads(request.data)
        video_id, title, description, for_channel = \
            data.get('video_id'), data.get('title'), data.get('description'), \
            _filter_for_channel(data.get('for_channel'))

        if video_id and title and description:
            sn_log.info("Trying to store video \n[%s]" % data)
            category = cat_retriever.get_category_of_video_title(title)
            sn_log.info("For [%s] implied [%s] category" % (video_id, category))
            if category:
                video_data = {"title": title, "description": description}
                if for_channel:
                    video_data['for_channel'] = for_channel
                    sn_log.info("Video [%s] will published only in this channels: %s" % (video_id, for_channel))

                result = sn_store.store_video(video_id, video_data, VT_TRACKED)
                if result:
                    sn_log.info("Store tracked video [%s]: %s" % (video_id, result))
                    sn_store.prepare_tracked_video(video_id, [category])
                    return jsonify(**{"ok": True, "upserted_id": str(result.upserted_id), "category": category})
                else:
                    sn_log.info("Error: at store video: [%s]" % video_id)
                    return jsonify(**{"ok": False, "error": "Video can not insert in db:("})
            else:
                sn_log.info("Error: can not imply category %s" % video_id)
                return jsonify(**{"ok": False, "error": "Can not imply category"})

        return jsonify(**{"ok": False, "error": "Can not find needed fields"})


class SnifferCategoryApi(MethodView):
    methods = ["POST", "GET", "DELETE", "PUT"]

    def post(self):
        name, words, phrases = request.form.get("name"), request.form.get("words"), request.form.get("phrases")
        if name and words:
            words = _retrieve_tokens(words)
            phrases = _retrieve_tokens(phrases)
            result = cfg_store.store_category(name, words, phrases)
            if 'error' in result:
                flash(result.get("error"))
            else:
                flash("OK")
        else:
            flash("no name or words or phrases")

        return redirect(url_for("sniffer_categories"))

    def put(self):
        data = json.loads(request.data)
        need_next, stop_words, split = data.get('need_next'), data.get('stop_words'), data.get('split')
        cfg_store.add_config(profile, {"need_next": _clear_words(need_next), "stop_words": _clear_words(stop_words),
                                       "split": split})
        return jsonify(**{"ok": True})

    def get(self):
        categories = cfg_store.get_all_categories()
        config = cfg_store.get_config(profile)
        cat_name = request.args.get("category_name")
        result = {"categories": categories, "config": config}
        if cat_name:
            category = cfg_store.get_category(cat_name)
            if category:
                result['category_change'] = category
        return render_template("sn_categories.html", **result)

    def delete(self, category_name):
        if category_name and cfg_store.get_category(category_name):
            cfg_store.delete_category(category_name)
            return jsonify(**{"ok": True})
        return jsonify(**{"ok": False})


class SnifferCheckCategoryApi(MethodView):
    methods = ['POST']

    def post(self):
        data = json.loads(request.data)
        title = data.get('title')
        if not title:
            return jsonify(**{"ok": False, "error": "No title provided :("})

        cat = cat_retriever.get_category_of_video_title(title)
        if not cat:
            return jsonify(**{"ok": False, "error": "No cat recognised :("})

        return jsonify(**{"ok": True, "category": cat})


def _clear_words(words):
    return filter(lambda x: x, map(lambda x: x.strip(), words.split()))


def _retrieve_tokens(string):
    return filter(lambda x: x, map(lambda x: x.strip(), string.split('\n')))


def _merge_videos(old, new):
    old_ids = dict(map(lambda x: (x.get("video_id"), x), old))
    new_ids = dict(map(lambda x: (x.get("video_id"), x), new))
    return dict(old_ids, **new_ids).values()
