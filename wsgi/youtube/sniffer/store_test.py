import time

from store import SnifferConfigStorage
from wsgi.youtube.sniffer import CategoriesRetriever

if __name__ == '__main__':
    scs = SnifferConfigStorage()
    scs.store_category("test1", ["a a a", "b", "c"], ["abc"])
    scs.store_category("test2", ["d", "e e", "f"], ["abc"])
    scs.store_category("test3", ["g g g g", "h", "j"], ["abc"])
    scs.store_category("test4", ["i", "j", "k"], ["abc"])

    st = time.time()
    for el in "abcdefghijk":
        print el, scs.get_category_of_word(el)
    print time.time() - st

    cr = CategoriesRetriever(storage=scs)
    print cr.get_category_of_video_title("a a a b h")
    print cr.get_category_of_video_title("fuck")
