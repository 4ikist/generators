import time

from pymongo import ASCENDING

from rr_lib.db import DBHandler

VT_TRACKED = 'tracked'
VT_PUBLISHED = 'published'
VT_ERROR = 'error'


class SnifferStorage(DBHandler):
    def __init__(self, name="?", ensure_indexes=False):
        super(SnifferStorage, self).__init__(name=name)
        if "sn_channels" not in self.collection_names or ensure_indexes:
            try:
                self.channels = self.db.create_collection("sn_channels")
            except:
                self.channels = self.db.get_collection("sn_channels")
                self.channels.drop_indexes()

            self.channels.create_index("channel_id", unique=True)
            self.channels.create_index("sniffed")
        else:
            self.channels = self.db.get_collection("sn_channels")

        if "sn_videos" not in self.collection_names or ensure_indexes:
            try:
                self.videos = self.db.create_collection("sn_videos")
            except:
                self.videos = self.db.get_collection("sn_videos")
                self.videos.drop_indexes()

            self.videos.create_index("video_id", unique=True)
            self.videos.create_index("categories", sparse=True)
            self.videos.create_index("type")
        else:
            self.videos = self.db.get_collection("sn_videos")

        self.meta = self.db.get_collection("sn_meta") or self.db.create_collection("sn_meta")

    def store_channel(self, channel_id, data):
        return self.channels.update_one({"channel_id": channel_id}, {"$set": data}, upsert=True)

    def get_channel(self, channel_id):
        return self.channels.find_one({"channel_id": channel_id})

    def delete_channel(self, channel_id):
        return self.channels.delete_one({"channel_id": channel_id})

    def get_channels_sniffed(self):
        return list(self.channels.find({"sniffed": True}))

    def set_channel_sniffed(self, channel_id, sniffed=True):
        self.channels.update_one({"channel_id": channel_id}, {"$set": {"sniffed": sniffed}}, upsert=True)

    def set_channel_last_video(self, channel_id, last_video_id):
        self.channels.update_one({"channel_id": channel_id}, {"$set": {"last_video_id": last_video_id}})

    def set_video_published(self, video_id, data=None):
        return self.videos.update_one({"video_id": video_id},
                                      {"$set": {"type": VT_PUBLISHED, "publish_data": data or {}}})

    def set_video_error(self, video_id, data=None):
        return self.videos.update_one({"video_id": video_id}, {"$set": {"type": VT_ERROR, "error_data": data or {}}})

    def store_video(self, video_id, video_data, type=VT_TRACKED):
        return self.videos.update_one({"video_id": video_id},
                                      {"$set": dict(video_data, **{"changed": time.time(), "type": type})},
                                      upsert=True)

    def get_videos_with_category(self, categories=None):
        q = {"type": VT_TRACKED}
        if categories is not None:
            q["categories"] = categories if isinstance(categories, list) else [categories]
        else:
            q['categories'] = {'$exists': True}

        q['for_channel'] = {"$exists": False}
        return list(self.videos.find(q).sort([("changed", ASCENDING)]))

    def get_videos_for_channel(self, channels=None):
        q = {"type": VT_TRACKED}
        if channels is not None:
            q['for_channel'] = channels if isinstance(channels, list) else [channels]
        else:
            q['for_channel'] = {"$exists": True}
        return list(self.videos.find(q).sort([("changed", ASCENDING)]))

    def get_videos_with_type(self, type):
        return list(self.videos.find({"type": type}))

    def get_video(self, video_id):
        return self.videos.find_one({"video_id": video_id})

    def delete_video(self, video_id):
        return self.videos.delete_one({'video_id': video_id})

    def prepare_tracked_video(self, video_id, categories):
        found = self.videos.find_one({"video_id": video_id})
        if found:
            if found.get("type") == VT_PUBLISHED:
                return

            return self.videos.update_one({"video_id": video_id},
                                          {"$addToSet": {"categories": {"$each": categories}},
                                           "$set": {"type": VT_TRACKED}},
                                          upsert=True)

    def set_current_channel_id(self, channel_id):
        self.meta.update_one({"current": "channel"}, {"$set": {"channel_id": channel_id}}, upsert=True)

    def get_current_channel_id(self):
        found = self.meta.find_one({"current": "channel"})
        if found:
            return found.get("channel_id")


class SnifferConfigStorage(DBHandler):
    CAT_COLLECTION_NAME = "sn_categories"
    WORD_COLLECTION_NAME = "sn_categories_words"
    CONFIG_COLLECTION_NAME = "sn_retrieve_config"

    def __init__(self, name="?"):
        super(SnifferConfigStorage, self).__init__(name=name)

        if self.CAT_COLLECTION_NAME not in self.collection_names:
            self.categories = self.db.create_collection(self.CAT_COLLECTION_NAME)
            self.categories.create_index("name", unique=True)
        else:
            self.categories = self.db.get_collection(self.CAT_COLLECTION_NAME)

        if self.WORD_COLLECTION_NAME not in self.collection_names:
            self.words = self.db.create_collection(self.WORD_COLLECTION_NAME)
            self.words.create_index("word", unique=True)
            self.words.create_index("cat_name")
        else:
            self.words = self.db.get_collection(self.WORD_COLLECTION_NAME)

        if self.CONFIG_COLLECTION_NAME not in self.collection_names:
            self.config = self.db.create_collection(self.CONFIG_COLLECTION_NAME)
            self.config.create_index("profile", unique=True)
        else:
            self.config = self.db.get_collection(self.CONFIG_COLLECTION_NAME)

    def store_category(self, name, words, phrases):
        lower_words = []
        for word in words:
            lower_word = word.lower()
            lower_words.append(lower_word)
            found = self.words.find_one({"word": lower_word})
            if not found:
                self.words.insert_one({"word": lower_word, "cat_name": name})
            else:
                return {"ok": False, "error": "have already word [%s] at [%s]" % (lower_word, found.get("cat_name"))}

        self.categories.update_one({"name": name}, {"$set": {"name": name, "words": lower_words, "phrases": phrases}},
                                   upsert=True)
        return {"ok": True}

    def get_category(self, name):
        return self.categories.find_one({"name": name})

    def get_all_categories(self):
        return list(self.categories.find({}))

    def delete_category(self, name):
        self.categories.delete_one({"name": name})
        self.words.delete_many({"cat_name": name})

    def get_words_to_category_mapping(self):
        words = self.words.find()
        result = {}
        for word in words:
            result[word['word']] = word['cat_name']
        return result

    def get_category_of_word(self, word, extended=True):
        found = self.words.find_one({"word": word})
        if found:
            if extended:
                return self.categories.find_one({"name": found.get("cat_name")})
            return found.get("cat_name")

    def add_config(self, profile, data):
        if not isinstance(data, dict):
            raise Exception("Bad data for sniffer retrieving config")

        self.config.update_one({"profile": profile}, {"$set": data}, upsert=True)

    def get_config(self, profile):
        return self.config.find_one({"profile": profile})
