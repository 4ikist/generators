from wsgi.youtube import YMStorage
from wsgi.youtube.engine import YoutubeManager
from wsgi.youtube.sniffer import Sniffer, get_auth_service
from wsgi.youtube.sniffer.store import SnifferStorage


class YoutubeManagerMock(YoutubeManager):
    def __init__(self, yt=None, channels=None, videos=None):
        super(YoutubeManagerMock, self).__init__(yt)
        self.channels = channels or {}
        self.videos = videos or {}

    def get_video_info(self, video_id):
        return self.videos.get(video_id)

    def get_channel_info(self, channel_id, before_video_id=None, add_videos=True):
        return self.channels.get(channel_id), [self.videos.get(channel_id)]


sn_storage = SnifferStorage()
ym_store = YMStorage("server")
ym = YoutubeManagerMock(
    channels={
        "1": {},
        "2": {}
    },
    videos={
        "1": {},
        "2": {}
    }
)


def test_sniffer_iterate():
    channel_id = sn_storage.get_current_channel_id()
    yt, _ = get_auth_service(ym_store, channel_id)
    sniffer = Sniffer(yt, ym=ym)

    sniffer.iterate()


if __name__ == '__main__':
    test_sniffer_iterate()