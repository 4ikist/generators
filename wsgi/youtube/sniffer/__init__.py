# coding=utf-8
import json
import logging
import os
import re
from collections import defaultdict
from datetime import datetime
from logging import FileHandler
from multiprocessing import Process
from time import sleep, time

import httplib2
from googleapiclient.discovery import build
from oauth2client.client import OAuth2Credentials

from rr_lib.cm import ConfigManager
from states.processes import ProcessDirector
from wsgi.youtube import YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION
from wsgi.youtube.engine import YoutubeManager
from wsgi.youtube.sniffer.core import CategoriesRetriever
from wsgi.youtube.sniffer.store import SnifferStorage, VT_PUBLISHED, SnifferConfigStorage

NAME = "sniffer"

DEFAULT_SOURCES_COUNT = 3

ASPECT_SNIFFER = NAME
ASPECT_SNIFFER_STOP = NAME + "_stop!"
SLEEP_SNIFFER_TIME = 3 * 60

sniffer_tick_time = 2

video_oldnes = 3600 * 2

cm = ConfigManager()
config = lambda name: (cm.get("sniffer") or {}).get(name)
log_file_name = os.path.join(
    os.path.dirname(os.path.dirname(__file__)),
    "result%s.log" % ("_%s" % NAME if NAME else ""))


def init_log():
    log = logging.getLogger(NAME)
    log.info("Init sniffer logging...")
    fh = FileHandler(log_file_name, mode='a')
    fmt = logging.Formatter('|%(asctime)s|%(message)s')
    fh.setFormatter(fmt)
    log.addHandler(fh)
    return log


def reset_log():
    with open(log_file_name, 'w') as f:
        f.write('\ntruncated: %s' % datetime.now())


def get_log_data():
    with open(log_file_name) as f:
        lines = f.readlines()
    return '<br>'.join(lines)


sn_log = init_log()
process_director = ProcessDirector(NAME)
re_description = re.compile("https?\:\/\/((www\.youtube\.com\/watch\?v=)|(youtu\.be\/))(.{11})")


def publish_video(youtube, video_id):
    cm = ConfigManager()
    if cm.get('sniffer').get('not_publish'):
        sn_log.info("I was not publish because that config")
        return {}
    try:
        status_update_response = youtube.videos().update(
            part='status',
            body={
                "status": {
                    'privacyStatus': 'public'
                },
                "id": video_id
            }
        ).execute()
    except Exception as e:
        sn_log.exception(e)
        status_update_response = {"error": str(e)}

    sn_log.info("Video [%s] published! Result:\n %s" % (video_id, json.dumps(status_update_response, indent=1)))
    return status_update_response


def update_video(youtube, video_id, new_title=None, new_description=None, new_tags=[], new_category_id=None):
    sn_log.info("Will update video: \n%s %s %s %s \n%s " % (
        video_id, new_title, ','.join(new_tags), new_category_id, new_description))

    videos_list_response = youtube.videos().list(
        id=video_id,
        part='snippet,status'
    ).execute()

    if not videos_list_response["items"]:
        sn_log.warning("Video '%s' was not found." % video_id)
        return

    video_snippet = videos_list_response["items"][0]["snippet"]

    if new_tags:
        video_snippet["tags"] = new_tags
    if new_description:
        video_snippet["description"] = new_description
    if new_title:
        video_snippet["title"] = new_title
    if new_category_id:
        video_snippet["categoryId"] = new_category_id

    video_snippet["defaultLanguage"] = 'en'

    sn_log.info("Will update snippet for video: [%s]" % (video_id))
    main_update_response = youtube.videos().update(
        part='snippet',
        body=dict(
            snippet=video_snippet,
            id=video_id
        )).execute()
    sn_log.info("Video updated! [%s]\n Result:\n %s" % (video_id, json.dumps(main_update_response, indent=1)))
    return main_update_response


class Sniffer(Process):
    def __init__(self, yt, storage=None, ym=None):
        super(Sniffer, self).__init__()
        self.name = NAME
        self.ym = ym or YoutubeManager(yt=yt)
        self.yt = yt or self.ym.youtube
        self.pd = process_director
        self.storage = storage or SnifferStorage(NAME)
        self.cats_retriever = CategoriesRetriever()

    @staticmethod
    def must_stop():
        sn_log.info("Will stopping...")
        process_director.redis.set(ASPECT_SNIFFER_STOP, time())

    def _check_aspect_work(self):
        started = self.pd.start_aspect(ASPECT_SNIFFER, tick_time=sniffer_tick_time)
        if not started:
            sn_log.info("Hmmm... may be anybody already works?")
            sleep(sniffer_tick_time + 1)
            started = self.pd.start_aspect(ASPECT_SNIFFER, tick_time=sniffer_tick_time)
            if not started:
                sn_log.info("Somehow worked... will end.")
                return
        return True

    def _check_must_stop(self):
        data = self.pd.redis.get(ASPECT_SNIFFER_STOP)
        if data:
            self.pd.redis.delete(ASPECT_SNIFFER_STOP)
            sn_log.info("Stop!")
            return True

    def retrieve_new_channel_videos(self, channel_ids):
        new_channel_videos = []
        for channel_id in channel_ids:
            channel = self.storage.get_channel(channel_id)
            if not channel:
                last_video_id = None
            else:
                last_video_id = channel.get("last_video_id")
            channel_info, new_videos = self.ym.get_channel_info(channel_id,
                                                                before_video_id=last_video_id,
                                                                add_videos=True)
            self.storage.store_channel(channel_id, channel_info)

            if new_videos:
                sn_log.info("For sniffed channel [%s] found new %s videos" % (channel_id, len(new_videos)))
                for video in new_videos:
                    video_id = video.get('video_id')
                    video = self.ym.get_video_info(video_id)
                    if not video:
                        sn_log.warning("Sniffed video [%s] is not found in yt :(" % video_id)
                        continue

                    if time() - video.get('time') > video_oldnes:
                        sn_log.warning("Sniffed video [%s] is so old for sniff him :( " % video_id)
                        continue

                    new_channel_videos.append(video)

                self.storage.set_channel_last_video(channel_id, new_videos[0].get("video_id"))

        return new_channel_videos

    def publish_video(self, video_id, context):
        publish_result = publish_video(self.yt, video_id)
        if publish_result:
            publish_result['context'] = context

            if 'error' in publish_result:
                sn_log.info("Error at publish video [%s]:( \n %s" % (video_id, publish_result))
                return self.storage.set_video_error(video_id, publish_result)

            sn_log.info("Published! [%s] \n%s" % (video_id, publish_result))
            return self.storage.set_video_published(video_id, publish_result)

    def publish_by_category_of_sniffed(self, our_videos, sniffed_videos, context):
        video_categories = self.cats_retriever.retrieve_categories(sniffed_videos, log=sn_log)
        our_cat_videos = self.group_by('categories', our_videos)

        for _, category in video_categories.iteritems():
            cat_videos = our_cat_videos.pop(category, None)
            if cat_videos is not None and isinstance(cat_videos, list):
                sn_log.info(
                    "For category [%s] found this our videos: \n%s" %
                    (category,
                     [x.get('video_id') for x in cat_videos])
                )

                for video in cat_videos:
                    self.publish_video(video.get('video_id'), dict({'published_category': category}, **context))

        if our_cat_videos:
            sn_log.info("Not published categories:\n%s" % our_cat_videos.keys())

    def group_by(self, by, videos):
        result = defaultdict(list)
        for video in videos:
            categories = video.get(by) or []
            for category in categories:
                result[category].append(video)
        return result

    def iterate(self):
        self.cats_retriever = CategoriesRetriever(storage=SnifferConfigStorage())

        videos_for_channel = self.storage.get_videos_for_channel(None)
        channels_new_videos = {}
        sniffed_videos = []

        if videos_for_channel:
            sn_log.info("Found %s our videos for all sniffed channels" % len(videos_for_channel))
            channel_videos_mapping = self.group_by('for_channel', videos_for_channel)
            for sniffed_channel, videos in channel_videos_mapping.iteritems():
                sn_log.info("For sniffed channel [%s] have %s our videos" % (sniffed_channel, len(videos)))

                new_channel_videos = channels_new_videos.get(sniffed_channel) or \
                                     self.retrieve_new_channel_videos([sniffed_channel])

                channels_new_videos[sniffed_channel] = new_channel_videos
                sniffed_videos.extend(new_channel_videos)

                if new_channel_videos:
                    sn_log.info("Will publish our videos for sniffed channel [%s]" % sniffed_channel)
                    self.publish_by_category_of_sniffed(videos, new_channel_videos,
                                                        {"sniffed_channel": sniffed_channel})

                    # videos_for_publish = self.storage.get_videos_with_category(None)
                    # if videos_for_publish:
                    #     sn_log.info("Found %s videos with category" % len(videos_for_publish))
                    #     another_sniffed_channels = filter(lambda x: x not in channels_new_videos,
                    #                                       map(lambda x: x.get('channel_id'),
                    #                                           self.storage.get_channels_sniffed()))
                    #     anew_sniffed_videos = self.retrieve_new_channel_videos(another_sniffed_channels)
                    #
                    #     if another_sniffed_channels:
                    #         sniffed_videos.extend(anew_sniffed_videos)
                    #         if sniffed_videos:
                    #             sn_log.info("Will publish our videos... Because have")
                    #             self.publish_by_category_of_sniffed(videos_for_publish, sniffed_videos, {})

    def run(self):
        if not self._check_aspect_work():
            sn_log.warning("Will stop because aspect is work.")
            return

        while 1:
            self.iterate()

            if self._check_must_stop():
                sn_log.info("Stopped...")
                return

            sleep(SLEEP_SNIFFER_TIME)


def get_auth_service(code_storage, client_id):
    stored_credentials = code_storage.get_oauth_credentials(client_id)
    if not stored_credentials:
        sn_log.info("For [%s] credentials are not found. Sniffer can not work." % client_id)
        return None, None

    sn_log.info("Found stored credentials for [%s]: \n%s" % (client_id, json.dumps(stored_credentials, indent=1)))

    credentials = OAuth2Credentials.from_json(json.dumps(stored_credentials))
    if credentials.access_token_expired:
        sn_log.info("Credentials are expired:(")
        try:
            credentials.refresh(httplib2.Http())
            code_storage.set_oauth_credentials(client_id, json.loads(credentials.to_json()))
            sn_log.info("Credentials was refreshed and stored...")
        except Exception as e:
            sn_log.error("Can not refresh token:(")
            sn_log.exception(e)
            return None, None
    try:
        http_auth = credentials.authorize(httplib2.Http())
        youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, http_auth)
    except Exception as e:
        sn_log.error("Can not authorise:(")
        sn_log.exception(e)
        return None, None
    try:
        channel_data = youtube.channels().list(mine=True, part='snippet,id').execute()
        sn_log.info("Getting mine channel data: %s" % channel_data)
        channel_data = channel_data.get('items', [{}])[0]
        if channel_data:
            channel = {}
            channel['channel_id'] = channel_data['id']
            channel['title'] = channel_data['snippet']['title']
            channel['description'] = channel_data['snippet']['description']
            return youtube, channel
        else:
            sn_log.warning("Can not get mine channel data, but yt engine is ok :(")
            return youtube, None
    except Exception as e:
        sn_log.error("Can not get mine channel data :(")
        sn_log.exception(e)
        return None, None
