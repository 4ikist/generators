# coding=utf-8
import logging
import re

from wsgi.youtube.sniffer.store import SnifferConfigStorage

MIN_TAG_LENGTH = 3
MIN_ANCHOR_WORDS_COUNT = 3
BEST_ANCHOR_WORDS_COUNT = 4
log = logging.getLogger("sn_core")


def get_only_words(string):
    return filter(lambda x: x, re.split(r'[\d\W]+', string))


def get_tokens(string):
    return filter(lambda x: x, re.split(r'[\W]+', string))


def n_grams(tokens, n):
    if n == 1:
        return tokens
    ngrams = []
    for i in range(len(tokens) - (n - 1)):
        ngrams.append(" ".join(tokens[i:i + n]))
    return ngrams


class CategoriesRetriever(object):
    def __init__(self, storage=None):
        self.storage = storage or SnifferConfigStorage("cat_retriever")
        self.video_cats_cache = {}

    def get_category_of_video_title(self, title):
        words_to_category_mapping = self.storage.get_words_to_category_mapping()
        tokens = get_only_words(title.lower())
        grams_matrix = [
            tokens,
            n_grams(tokens, 2),
            n_grams(tokens, 3),
            n_grams(tokens, 4)
        ]
        token_pos = 0
        grams_pos = 0
        while 1:
            if grams_pos < len(grams_matrix):
                gram = grams_matrix[grams_pos]
            else:
                return

            if token_pos < len(gram):
                token = gram[token_pos]
            else:
                token_pos = 0
                grams_pos += 1
                continue

            if token in words_to_category_mapping:
                return words_to_category_mapping[token]
            else:
                if grams_pos >= len(grams_matrix):
                    grams_pos = 0
                    token_pos += 1
                elif token_pos >= len(gram):
                    grams_pos += 1
                    token_pos = 0
                else:
                    token_pos += 1

    def retrieve_categories(self, videos, log=log):
        video_cats = {}
        log.info("Will retrieve categories for: \n%s" % ", ".join(map(lambda x: x.get('video_id'), videos)))
        for video in videos:
            cat = self.video_cats_cache.get(video.get('video_id')) or \
                  self.get_category_of_video_title(video.get("title"))
            if cat:
                video_cats[video.get('video_id')] = cat
                log.info("[%s] => [%s]" % (video.get('video_id'), cat))

        self.video_cats_cache = dict(self.video_cats_cache, **video_cats)
        return video_cats


def levenshtein_distance(s1, s2):
    if len(s1) > len(s2):
        s1, s2 = s2, s1

    distances = range(len(s1) + 1)
    for i2, c2 in enumerate(s2):
        distances_ = [i2 + 1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
    return distances[-1]


def string_differences(s1, s2):
    lev = float(levenshtein_distance(s1, s2))
    if lev == 0:
        return 0
    else:
        return lev / (len(s1) + len(s2))


splitter = re.compile("[^a-zA-Z0-9&']")
need_next_word = ['&', 'vs', 'and', 'or', 'plus', 'minus']
stop_words = ['ep', 'episode', u'№']


class AnchorRetriever(object):
    def __init__(self, storage=None, profile='default'):
        self.storage = storage or SnifferConfigStorage("cat_retriever")
        self.profile = profile
        self.config = self.storage.get_config(self.profile)

    def _get_grams(self, title_tokens, n, acc):
        if n not in acc:
            acc[n] = n_grams(title_tokens, n)
        return acc[n]

    def capitalize_title(self, title):
        result = []
        for el in title.split():
            if len(el) > MIN_TAG_LENGTH:
                result.append(el.capitalize())
            else:
                result.append(el.lower())
        return " ".join(result)

    def cmp_parts_length(self, x, y):
        f = lambda x: abs(BEST_ANCHOR_WORDS_COUNT - len(x['part'].split()))
        if f(x) < f(y):
            return -1
        if f(x) > f(y):
            return 1
        return 0

    def cmp(self, x, y):
        parts_result = self.cmp_parts_length(x, y)
        if parts_result == 0:
            if x['diff'] < y['diff']:
                return -1
            if x['diff'] > y['diff']:
                return 1
            if x['diff'] == y['diff']:
                return 0
        return parts_result

    def get_anchor_without_tags(self, title, splitter, need_next, stop_words):
        log.info("Get anchor without tags")
        title_words = splitter.split(title)
        last_word = None
        anchor_words_count = 0
        wait_next = False

        for word in title_words:
            if word:
                if word.lower() in stop_words:
                    break

                if word.lower() in need_next:
                    wait_next = True
                else:
                    wait_next = False

                if len(word) >= MIN_TAG_LENGTH:
                    anchor_words_count += 1

                last_word = word

            if anchor_words_count >= MIN_ANCHOR_WORDS_COUNT and not wait_next:
                break

        if last_word:
            return title[:title.rfind(last_word) + len(last_word)]

    def check_gram(self, gram, stop_words, need_next):
        gram_words = gram.lower().split()
        gram_words_set = set(gram_words)
        if not len(gram_words_set.intersection(stop_words)):
            if gram_words[0] not in need_next and gram_words[-1] not in need_next:
                if len(gram_words) == len(gram_words_set):
                    return True

    def get_anchor(self, video):
        config = self.storage.get_config(self.profile)
        splitter = re.compile(config.get('split'))
        need_next = config.get('need_next')
        stop_words = config.get('stop_words')

        try:
            grams = {}
            tags = video.get("tags")
            title = video.get("title")
            if not tags:
                return self.get_anchor_without_tags(title, splitter, need_next, stop_words)

            title_tokens = get_tokens(title)
            similarities = []
            for tag in tags:
                if len(tag) < MIN_TAG_LENGTH:
                    continue
                tag_tokens = get_tokens(tag)
                tag_tokens_count = len(tag_tokens)
                tag_tokens_str = " ".join(tag_tokens)
                if tag_tokens_count < len(title_tokens):
                    title_grams = self._get_grams(title_tokens, tag_tokens_count, grams)
                    best_sim = len(title)
                    best_gram = None
                    for title_gram in title_grams:
                        sim = string_differences(title_gram, tag_tokens_str)
                        if sim < best_sim and self.check_gram(title, stop_words, need_next):
                            best_sim = sim
                            best_gram = title_gram

                    if best_gram:
                        similarities.append({"diff": best_sim, "part": best_gram})

            if similarities:
                similarities = sorted(similarities, cmp=self.cmp)
                for similarity in similarities:
                    if similarity and similarity.get('part'):
                        bs_tokens = get_tokens(similarity['part'])
                        last_token = bs_tokens[-1]
                        index = title.find(last_token) + len(last_token)
                        return self.capitalize_title(title[:index])

            return self.get_anchor_without_tags(title, splitter, need_next, stop_words)

        except Exception as e:
            log.error("Error at getting anchor of %s" % video)
            log.exception(e)
        return None


if __name__ == '__main__':
    ar = AnchorRetriever()
    video_ids = [
        "xnuBiEbUh_k",
        "6xnCd74w6kk",
        "LQa2nX2rKDA",
        "6U-hS63KY_A",
        "n-RjRfSqX5A",
        "5k3MXI5Kow0",
        "nlMM4Mj2QoA"
    ]

    from wsgi.youtube.engine import YoutubeManager

    ym = YoutubeManager()
    for video_id in video_ids:
        video = ym.get_video_info(video_id)
        if not video:
            print video_id, "not video url :("
            continue
        anchor = ar.get_anchor(video)
        print video.get("title"), ">>>>>", anchor
