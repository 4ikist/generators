import logging
import sys
import time
import traceback
from collections import defaultdict
from datetime import datetime
from multiprocessing import Process
from threading import Event, Thread, RLock

import redis
from googleapiclient.discovery import build
from stemming.porter2 import stem

from rr_lib.cm import ConfigManager
from states.processes import ProcessDirector
from wsgi.youtube import YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION
from wsgi.youtube.engine import token_reg, stop_words_en
from wsgi.youtube.store import YMStorage

log = logging.getLogger("experiments")

to_engine_start = "TO_ENGINE_START"
to_engine_stop = "TO_ENGINE_STOP"
to_result = lambda x: "TO_RESULT_%s" % x

s_end = 'end'
s_error = 'error'

DEFAULT_TIME_TO_SEE = 3600 * 24 * 7

bad_words = {"http", "https", "com", "info", "play", "game", "www", "follow", "find", }

__doc__ = """
c_filter =
'commentCount_f'
'commentCount_t'

'subscriberCount_f'
'subscriberCount_t'

'videoCount_f'
'videoCount_t'

'viewCount_f'
'viewCount_t'


 v_filter =
'commentCount_f'
'commentCount_t'

'dislikeCount_f'
'dislikeCount_t'

'favoriteCount_f'
'favoriteCount_t'

'likeCount_f'
'likeCount_t'

'viewCount_f'
'viewCount_t'


'time_to_see'


agg_filter=
w_f
w_t

t_f
t_f
"""


def calc_avg_speed(speeds):
    return sum(speeds) / float(len(speeds))


def calc_rate(statistics):
    return float(statistics.get("viewCount", 1))


def calc_speed(video_data):
    return video_data.get("rate") / video_data.get("age")


def normalize(sentence, bad_words=bad_words):
    res = set()
    if isinstance(sentence, (str, unicode)):
        tokens = token_reg.split(sentence.lower().strip())
        for token in tokens:
            if len(token) > 2 and token not in bad_words:
                stemmed = stem(token)
                if stemmed not in stop_words_en:
                    res.add(token)
    return res


def aggregate_videos_datas(videos_datas):
    tags_speeds, words_speeds = defaultdict(list), defaultdict(list)
    for video_data in videos_datas:
        v_speed = calc_speed(video_data)
        if not video_data.get("words") or not video_data.get("tags"):
            log.info("not tags or words in: %s" % video_data.get("video_id"))
            continue

        for tag in video_data.get("tags"):
            tags_speeds[tag].append(v_speed)
        for word in video_data.get("words"):
            words_speeds[word].append(v_speed)

    tags_result, words_result = {}, {}

    for tag, speeds in tags_speeds.iteritems():
        tags_result[tag] = calc_avg_speed(speeds)

    for word, speeds in words_speeds.iteritems():
        words_result[word] = calc_avg_speed(speeds)

    return tags_result, words_result


def recalculate(one_channel, another_channel):
    # print one_channel, another_channel
    return one_channel


def aggregate_channels_datas(channels_datas):
    tags_speeds, words_speeds = defaultdict(list), defaultdict(list)
    for tags, words in channels_datas:
        for tag, speed in tags.iteritems():
            tags_speeds[tag].append(speed)
        for word, speed in words.iteritems():
            words_speeds[word].append(speed)

    tags_result, words_result = {}, {}

    for tag, speeds in tags_speeds.iteritems():
        tags_result[tag] = calc_avg_speed(speeds)

    for word, speeds in words_speeds.iteritems():
        words_result[word] = calc_avg_speed(speeds)

    return tags_result, words_result


class YoutubeFilterDataManager(object):
    def __init__(self, yt=None):
        cm = ConfigManager()
        self.youtube = yt or build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                                   developerKey=cm.get('YOUTUBE_DEVELOPER_KEY'))
        self.store = YMStorage(name="youtube manager")

    def get_ids_of_channels(self, filter_data):
        q = {
            'q': filter_data.get('keywords'),
            'part': "id,snippet",
            'maxResults': 50,
            'order': 'viewCount',
            'type': 'channel,video'
        }
        channel_id = lambda channel_info: channel_info.get("snippet", {}).get("channelId")
        for channels_bach in self._search_iter(q):
            channels_datas = []
            loaded_channel_ids = list(set(map(channel_id, filter(channel_id, channels_bach))))
            stored, addled = self.store.get_addled_channels(loaded_channel_ids)
            channels_datas.extend(stored)

            if addled:
                updated_channels_datas = self._load_new_channels_datas(addled, filter_data)
                channels_datas.extend(updated_channels_datas)

            for channels_data in channels_datas:
                yield channels_data.get("channel_id"), channels_data

    def _load_new_channels_datas(self, channel_ids, filter_data):
        response = self.youtube.channels().list(part="statistics,snippet", id=",".join(channel_ids)).execute()
        items = response.get('items', [])
        result = []
        for channel_data in items:
            statistics = channel_data.get("statistics")
            snippet = channel_data.get("snippet")
            if not statistics or not snippet or \
                    not self._check_stat(statistics, filter_data.get('c_filter')) or \
                    not self._check_words(" ".join([snippet.get("title"), snippet.get("description")]),
                                          filter_data.get("m_keywords")):
                continue
            channel_id = channel_data.get("id")
            info = dict(statistics, **snippet)
            self.store.update_channel(channel_id, info, filter_data.get('exp_id'))
            result.append(dict({"channel_id": channel_id}, **info))
        return result

    def get_channel_videos(self, channel_id, exp_data=None):
        log.info("Start find videos for %s" % channel_id)
        if exp_data:
            v_filter, time_to_see = exp_data['v_filter'], exp_data['time_to_see']
        else:
            v_filter, time_to_see = None, None

        q = {
            "channelId": channel_id,
            "type": "video",
            "part": "snippet",
            "maxResults": 50,
            "order": "viewCount"
        }
        pub_at = lambda video_info: video_info.get("snippet", {}).get("publishedAt")
        video_id = lambda video_info: video_info.get("id", {}).get("videoId")

        for video_bach in self._search_iter(q):
            videos_datas = []
            new_video_ids = map(lambda x: x[0],
                                filter(lambda x: (time.time() - x[1] < time_to_see) if time_to_see else True,
                                       map(lambda video_info: (video_id(video_info),
                                                               time.mktime(datetime.strptime(pub_at(video_info),
                                                                                             "%Y-%m-%dT%H:%M:%S.000Z").timetuple())),
                                           filter(lambda x: pub_at(x) and video_id(x), video_bach))))

            stored, addled = self.store.get_addled_videos(new_video_ids)
            videos_datas.extend(stored)
            if addled:
                new_videos_datas = self._load_new_videos_datas(addled, v_filter)
                videos_datas.extend(new_videos_datas)

            for video_data in videos_datas:
                yield video_data

    def _load_new_videos_datas(self, video_ids, v_filter):
        cur_load_videos_data = {}
        while 1:
            try:
                cur_load_videos_data = self.youtube.videos().list(
                    **{"id": ",".join(video_ids), "part": "snippet,statistics"}).execute()
                break
            except Exception as e:
                log.warning(e)
                time.sleep(5)

        load_videos_data = cur_load_videos_data.get("items", [])
        videos_datas = []
        for video in load_videos_data:
            snippet = video.get("snippet", {})
            statistics = video.get("statistics", {})
            if snippet and statistics and self._check_stat(statistics, v_filter):
                publishedAt = snippet.get("publishedAt")
                video_data = {"tags": snippet.get("tags"),
                              "words": list(normalize(" ".join([snippet.get("title"), snippet.get("description")]))),
                              "age": time.time() - time.mktime(
                                  datetime.strptime(publishedAt, "%Y-%m-%dT%H:%M:%S.000Z").timetuple()),
                              "rate": calc_rate(statistics),
                              "statistics": statistics,
                              "video_id": video.get("id"),
                              "channel_id": snippet.get("channelId"),
                              "thumb": snippet.get("thumbnails", {}).get("default", {}).get("url"),
                              "title": snippet.get("title"),
                              "description": snippet.get("description")
                              }
                self.store.update_video(video.get("id"), video_data)
                videos_datas.append(video_data)
        return videos_datas

    def _check_words(self, sentence, minus_words):
        return len(normalize(sentence).intersection(set(minus_words))) == 0

    def _check_stat(self, stat, filter_data):
        if filter_data is None:
            return True
        for k, v in stat.iteritems():
            f_k = "%s_f" % k
            t_k = "%s_t" % k
            if f_k in filter_data and t_k in filter_data:
                v = int(v)
                if v < filter_data[f_k] or v > filter_data[t_k]:
                    return False
                else:
                    stat[k] = v
        return True

    def _search_iter(self, q):
        while 1:
            search_result = self.youtube.search().list(**q).execute()
            items = search_result.get("items", [])
            if items:
                yield items
            else:
                break

            if not search_result.get("nextPageToken"):
                break
            else:
                q['pageToken'] = search_result.get("nextPageToken")


class ExperimentDataBroker():
    def __init__(self):
        cm = ConfigManager()
        self.redis = redis.StrictRedis(host=cm.get("ym_redis_address"),
                                       port=int(cm.get("ym_redis_port")),
                                       password=cm.get("ym_redis_password"))

        self.ym_store = YMStorage(name="experiment data broker")

        self.to_engine_start_pubsub = self.redis.pubsub(ignore_subscribe_messages=True)
        self.to_engine_start_pubsub.subscribe(to_engine_start)

    def get_experiment_stop(self):
        pubsub = self.redis.pubsub(ignore_subscribe_messages=True)
        pubsub.subscribe(to_engine_stop)
        for message in pubsub.listen():
            exp_id = message.get("data")
            log.info("Will stop experiment > %s" % exp_id)
            yield exp_id

    def stop_experiment(self, exp_id):
        log.info("Will stop experiment < %s" % exp_id)
        self.redis.publish(to_engine_stop, exp_id)

    def get_experiment_start(self):
        for message in self.to_engine_start_pubsub.listen():
            exp_id = message.get("data")
            exp_data = self.ym_store.get_experiment_data(exp_id)
            log.info("Will start experiment %s > " % exp_id)
            yield exp_id, exp_data

    def start_experiment(self, keywords, m_keywords, c_filter, v_filter, time_to_see, agg_filter):
        exp_id, _ = self.ym_store.new_experiment(keywords, m_keywords, c_filter, v_filter, time_to_see,
                                                 agg_filter)
        log.info("Will start experiment %s <" % exp_id)
        self.redis.publish(to_engine_start, exp_id)
        return exp_id

    def publish_experiment_result(self, exp_id, result_state_id, data):
        self.ym_store.add_result(exp_id, result_state_id, data)
        self.redis.lpush(to_result(exp_id), result_state_id)

    def get_experiment_results(self, exp_id):
        while 1:
            result_state_id = self.redis.rpop(to_result(exp_id))
            if not result_state_id:
                break
            data = self.ym_store.pop_result(exp_id, result_state_id)
            yield result_state_id, data


def from_dict_to_list(d):
    result = []
    for k, v in d.iteritems():
        result.append({"k": k, "v": v})
    return result


class ExperimentsProcess(Process):
    aspect = "ym_exp_process"

    def __init__(self):
        super(ExperimentsProcess, self).__init__()
        log.info("Init experiment process")
        self.broker = ExperimentDataBroker()
        self.pd = ProcessDirector("experiment process")
        self.ym = YoutubeFilterDataManager()

        self.daemon = True
        self._events = {}
        self._mu = RLock()
        self._th = Thread(target=self._stop, args=(self._get_event,))
        self._th.setDaemon(True)
        self._th.start()
        log.info("Experiment process init!")

    def publish_results(self, exp_id, state, tags=None, words=None, channel_data=None, error=None):
        to_save = {}
        if tags and words:
            to_save = {"tags": from_dict_to_list(tags), "words": from_dict_to_list(words)}

        if isinstance(channel_data, dict):
            to_save = dict(to_save, **channel_data)
            _state = state
        elif error:
            to_save["error_info"] = error
            _state = s_error
        else:
            to_save["channels"] = channel_data
            _state = s_end

        log.info("Publish result: %s %s" % (exp_id, state))
        self.broker.publish_experiment_result(exp_id, _state, to_save)

    def aggregate_channel(self, channel_id, exp_id):
        exp_data = self.ym.store.get_experiment_data(exp_id)
        tags, words = aggregate_videos_datas(self.ym.get_channel_videos(channel_id, exp_data))
        return tags, words

    def load_channel_exp_results(self, exp_id, exp_data, channel_count, event):
        tags_and_words = []
        for channel_id, channel_info in self.ym.get_ids_of_channels(exp_data):
            try:
                tags, words = aggregate_videos_datas(self.ym.get_channel_videos(channel_id, exp_data))
                log.info("Load channel: %s %s %s" % (channel_id, len(tags), len(words)))
                channel_count += 1
                self.publish_results(exp_id, channel_id, tags, words, channel_info)
                if len(tags) and len(words):
                    tags_and_words.append((tags, words))

                yield (tags, words)

            except Exception as e:
                log.exception(e)
                self.publish_results(exp_id, "error", error=e)

            if event.isSet():
                log.info("Event was setted!")
                break

        log.info("Stopping %s", exp_id)
        c_tags, c_words = aggregate_channels_datas(tags_and_words)
        self.publish_results(exp_id, "end", tags=c_tags, words=c_words, channel_data=channel_count)

    def _work(self, exp_id, exp_data, event):
        log.info("Start work for %s" % exp_id)
        try:
            channel_count = 0
            tags_result, word_result = aggregate_channels_datas(
                self.load_channel_exp_results(
                    exp_id,
                    exp_data,
                    channel_count,
                    event
                )
            )
            self.publish_results(exp_id, "end", tags_result, word_result, channel_count)
        except Exception as e:
            log.exception(e)
            _, _, tb = sys.exc_info()
            self.publish_results(exp_id, s_error,
                                 error="error: %s\n%s" % (e.message, " ".join(traceback.format_tb(tb))))

    def _get_event(self, exp_id):
        with self._mu:
            return self._events.get(exp_id)

    def _stop(self, get_event):
        for stop_exp_id in self.broker.get_experiment_stop():
            event = get_event(stop_exp_id)
            if event:
                log.info("Will stop %s" % stop_exp_id)
                event.set()

    def start_experiment(self, exp_id, exp_data):
        self._events = dict(filter(lambda x: not x[1].isSet(), [(k, v) for k, v in self._events.items()]))
        if len(self._events) >= 2:
            self.publish_results(exp_id,
                                 s_error,
                                 error="Stop another experiments: %s, and try later" % self._events.keys())
            return

        event = Event()
        th_work = Thread(target=self._work, args=(exp_id, exp_data, event))
        th_work.setDaemon(True)
        th_work.start()
        self._events[exp_id] = event

    def run(self):
        self.pd.start_aspect(self.aspect, 30)

        log.info("Will start listen broker")

        for exp_id, exp_data in self.broker.get_experiment_start():
            log.info("Have experiment [%s]: %s" % (exp_id, exp_data))
            self.start_experiment(exp_id, exp_data)
            log.info("Experiment %s was started..." % exp_id)


def get_data_from_input_or_default(form):
    result = {
        'c_filter': {
            'commentCount_f': int(form.get("ch_commentCount_f", 10)),
            'commentCount_t': int(form.get("ch_commentCount_t", 10000000000)),

            'subscriberCount_f': int(form.get("subscriberCount_f", 100)),
            'subscriberCount_t': int(form.get("subscriberCount_t", 10000000000)),

            'videoCount_f': int(form.get("videoCount_f", 100)),
            'videoCount_t': int(form.get("videoCount_t", 10000000000)),

            'viewCount_f': int(form.get("ch_viewCount_f", 100000)),
            'viewCount_t': int(form.get("ch_viewCount_t", 10000000000))
        },
        'v_filter': {
            'commentCount_f': int(form.get("vid_commentCount_f", 0)),
            'commentCount_t': int(form.get("vid_commentCount_t", 10000000000)),

            'dislikeCount_f': int(form.get("dislikeCount_f", 0)),
            'dislikeCount_t': int(form.get("dislikeCount_t", 10000000000)),

            'favoriteCount_f': int(form.get("favoriteCount_f", 0)),
            'favoriteCount_t': int(form.get("favoriteCount_t", 10000000000)),

            'likeCount_f': int(form.get("likeCount_f", 0)),
            'likeCount_t': int(form.get("likeCount_t", 10000000000)),

            'viewCount_f': int(form.get("vid_viewCount_f", 0)),
            'viewCount_t': int(form.get("vid_viewCount_t", 10000000000))
        },
        'a_filter': {
            'w_f': int(form.get("w_f", 0)),
            'w_t': int(form.get("w_t", 999999999)),

            't_f': int(form.get("t_f", 0)),
            't_t': int(form.get("t_t", 999999999))
        },
        "time_to_see": form.get("time_to_see") or DEFAULT_TIME_TO_SEE,
        "keywords": form.get("keywords", "funny|porn"),
        "m_keywords": (form.get("m_keywords") or "").strip().split(),
    }
    return result

# if __name__ == '__main__':
#     b = ExperimentDataBroker()
#
#     ep = ExperimentsProcess()
#     ep.start()
#
#     exp_id = b.start_experiment(**get_data_from_input_or_default({}))
#
#     count = 10
#     while 1:
#         for result in b.get_experiment_results(exp_id):
#             print result
#             count += 1
#         if count > 10:
#             break
#
#     b.stop_experiment(exp_id)
#
#     ep.join()
