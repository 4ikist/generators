#!/usr/bin/python
import datetime
import logging
import os
from time import mktime, sleep

from apiclient.errors import HttpError
from oauth2client import clientsecrets
from oauth2client.client import OAuth2WebServerFlow

from rr_lib.cm import ConfigManager
from wsgi.properties import YOUTUBE_CLIENT_SECRET
from wsgi.youtube.store import YMStorage

CLIENT_SECRETS_FILE = YOUTUBE_CLIENT_SECRET

# This OAuth 2.0 access scope allows for full read/write access to the
# authenticated user's account.
YOUTUBE_SCOPE = "https://www.googleapis.com/auth/youtube"
YOUTUBE_UPLOAD_SCOPE = "https://www.googleapis.com/auth/youtube.upload"
YOUTUBE_READ_WRITE_SSL_SCOPE = "https://www.googleapis.com/auth/youtube.force-ssl"
YOUTUBE_CHANNEL_AUDIT = "https://www.googleapis.com/auth/youtubepartner-channel-audit"
YOUTUBE_PARTNER = "https://www.googleapis.com/auth/youtubepartner"

YOUTUBE_READ_WRITE_SCOPE = "https://www.googleapis.com/auth/youtubepartner-channel-audit"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

# This variable defines a message to display if the CLIENT_SECRETS_FILE is
# missing.
MISSING_CLIENT_SECRETS_MESSAGE = lambda fn: """
WARNING: Please configure OAuth 2.0

To make this sample run you will need to populate the client_secrets.json file
found at:

   %s...

with information from the {{ Cloud Console }}
{{ https://cloud.google.com/console }}

For more information about the client_secrets.json file format, please visit:
https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
""" % os.path.abspath(os.path.join(os.path.dirname(__file__),
                                   fn))

cm = ConfigManager()
log = logging.getLogger("yt_init")

AM_INSTALL = 'install'
AM_WEB = 'web'


def yt_date_to_tst(yt_date):
    return mktime(datetime.datetime.strptime(yt_date, "%Y-%m-%dT%H:%M:%S.000Z").timetuple())


def get_flow():
    client_type, client_info = clientsecrets.loadfile("%s_%s" % (CLIENT_SECRETS_FILE, "web"))
    scope = [YOUTUBE_SCOPE, YOUTUBE_READ_WRITE_SCOPE, YOUTUBE_UPLOAD_SCOPE, YOUTUBE_CHANNEL_AUDIT, YOUTUBE_PARTNER]
    redirect_uri = cm.get("ym_redirect_uri")

    flow = OAuth2WebServerFlow(
        client_info['client_id'],
        client_info['client_secret'],
        scope,
        # prompt='consent',
        approval_prompt='force',
        redirect_uri=redirect_uri,
        auth_uri=client_info['auth_uri'],
        token_uri=client_info['token_uri'],
        login_hint=None,

    )

    flow.params['access_type'] = 'offline'  # offline access
    flow.params['include_granted_scopes'] = 'true'  # incremental auth
    log.info("Creating flow: \n%s" % flow)
    return flow


tryings_count = 3
sleep_between_try = 2


def net_tryings(fn):
    def wrapped(*args, **kwargs):
        count = 0
        while 1:
            try:
                result = fn(*args, **kwargs)
                return result
            except HttpError as e:
                if e.resp.get('status') in ("400", "403", "404"):
                    return None
            except Exception as e:
                log.exception(e)
                log.warning("can not load data for [%s]\n args: %s, kwargs: %s \n because %s" % (fn, args, kwargs, e))
                if count >= tryings_count:
                    raise e
                sleep(sleep_between_try)
                count += 1

    return wrapped


def next_page_iter(list_func, q):
    while 1:
        result = net_tryings(list_func(**q).execute)()
        if not result:
            break

        items = result.get("items", [])
        if items:
            for item in items:
                yield item
        else:
            break

        if not result.get("nextPageToken"):
            break
        else:
            q['pageToken'] = result.get("nextPageToken")
