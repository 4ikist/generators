import time
import uuid

from rr_lib.cm import ConfigManager
from rr_lib.db import DBHandler

CHANNEL_ADDLED_TIME = 60 * 60
VIDEO_ADDLED_TIME = 60 * 10
DEFAULT_TIME_TO_SEE = 3600 * 24 * 7


def _exp_id(keywords, minus_keywords):
    return uuid.uuid5(uuid.NAMESPACE_DNS, str(hash("".join([keywords] + minus_keywords)))).hex


class YMStorage(DBHandler):
    filter_c_n = "ytm_filters"
    channels_c_n = "ytm_channels"
    videos_c_n = "ytm_videos"
    results_c_n = "ytm_exp_results"
    oauth_data_c_n = "ytm_oauth_data"

    def __init__(self, name=None):
        cm = ConfigManager()
        super(YMStorage, self).__init__(name="YM_%s" % name if name else "YM",
                                        uri=cm.get("ym_mongo_uri"),
                                        db_name=cm.get("ym_db_name"))
        if self.filter_c_n not in self.collection_names:
            self.ym_experiments = self.db.create_collection(self.filter_c_n)
            self.ym_experiments.create_index("exp_id", unque=True)
        else:
            self.ym_experiments = self.db.get_collection(self.filter_c_n)

        if self.channels_c_n not in self.collection_names:
            self.ym_channels = self.db.create_collection(self.channels_c_n, capped=True, size=1024 * 1024)
            self.ym_channels.create_index("channel_id", unique=True)
        else:
            self.ym_channels = self.db.get_collection(self.channels_c_n)

        if self.videos_c_n not in self.collection_names:
            self.ym_videos = self.db.create_collection(self.videos_c_n, capped=True, size=1024 * 1024 * 100)
            self.ym_videos.create_index("video_id", unique=True)
        else:
            self.ym_videos = self.db.get_collection(self.videos_c_n)

        if self.results_c_n not in self.collection_names:
            self.results = self.db.create_collection(self.results_c_n, capped=True, size=1024 * 200)
            self.results.create_index([("result_id", 1), ("exp_id", 1), ("a_time", 1)], unique=True)
            self.results.create_index([("exp_id", 1)])
            self.results.create_index("cached", sparse=True)
        else:
            self.results = self.db.get_collection(self.results_c_n)

        if self.oauth_data_c_n not in self.collection_names:
            self.oauth_data = self.db.create_collection(self.oauth_data_c_n)
            self.oauth_data.create_index("channel_id", unique=True)
        else:
            self.oauth_data = self.db.get_collection(self.oauth_data_c_n)

    def _check_ids(self, col, ids, id_name):
        found = col.find({id_name: {"$in": ids}})
        found = dict(map(lambda x: (x.get(id_name), x),
                         filter(lambda x: (time.time() - x.get("toggle_time")) > CHANNEL_ADDLED_TIME,
                                found)))

        not_found_ids = set(ids).difference(found.keys())
        found_data = map(lambda x: x[1], filter(lambda x: x[0] not in not_found_ids, found.items()))
        return found_data, not_found_ids

    def get_channel_info(self, channel_id):
        return self.ym_channels.find_one({"channel_id": channel_id}, projection={"_id": False})

    def get_channels_info(self, channels_ids):
        return self.ym_channels.find({"channel_id": {"$in": channels_ids}}, projection={"_id": False})

    def new_experiment(self, keywords, m_keywords, c_filter, v_filter, time_to_see, a_filter):
        exp_id = _exp_id(keywords, m_keywords)

        filter_store = {
            "exp_id": exp_id,
            "time_to_see": time_to_see,

            "keywords": keywords,
            "m_keywords": m_keywords,

            "c_filter": c_filter,
            "v_filter": v_filter,

            "a_filter": a_filter,
        }

        self.ym_experiments.update_one({"exp_id": exp_id}, {"$set": filter_store}, upsert=True)
        return exp_id, filter_store

    def get_experiment_data(self, exp_id, extend=False):
        found = self.ym_experiments.find_one({"exp_id": exp_id}, projection={"_id": False})
        return found

    def get_all_experiments_data(self):
        results_counts = self.get_exps_results_counts()
        exps_datas = self.ym_experiments.find({}, projection={"_id": False})
        result = {}
        for exp_data in exps_datas:
            result[exp_data.get("exp_id")] = dict({"results_count": results_counts.get(exp_data.get("exp_id"), 0)},
                                                  **exp_data)
        return result

    def delete_exp(self, exp_id):
        self.ym_experiments.delete_one({"exp_id": exp_id})

    def update_channel(self, channel_id, channel_data, exp_id):
        to_set = dict({"toggle_time": time.time(), "channel_id": channel_id}, **channel_data)
        self.ym_channels.update_one({"channel_id": channel_id}, {"$set": to_set}, upsert=True)
        self.add_channel_to_experiment(exp_id, channel_id)

    def add_channel_to_experiment(self, exp_id, channel_id):
        self.ym_experiments.update_one({"exp_id": exp_id}, {"$addToSet": {"channels": channel_id}})

    def get_addled_channels(self, channel_ids):
        return self._check_ids(self.ym_channels, channel_ids, "channel_id")

    def update_video(self, video_id, video_data):
        to_set = dict({"toggle_time": time.time(), "video_id": video_id}, **video_data)
        self.ym_videos.update_one({"video_id": video_id}, {"$set": to_set}, upsert=True)

    def get_addled_videos(self, video_ids):
        return self._check_ids(self.ym_videos, video_ids, "video_id")

    def add_result(self, exp_id, result_id, data):
        if result_id == 'end':
            return self.results.update_one({"exp_id": exp_id},
                                           {"$set": dict({"a_time": time.time(), "result_id": result_id,}, **data)}
                                           )
        else:
            return self.results.insert_one(
                dict({"exp_id": exp_id, "result_id": result_id, "a_time": time.time()}, **data)
            )

    def pop_result(self, exp_id, result_id):
        pop_q = {"exp_id": exp_id, "result_id": result_id}
        found = self.results.find_one(pop_q)
        return found

    def get_exps_results_counts(self):
        result = {}
        for exp_result in self.results.aggregate([{"$group": {"_id": "$exp_id", "results_count": {"$sum": 1}}}]):
            result[exp_result.get("_id")] = exp_result.get("results_count")
        return result

    def set_oauth_credentials(self, channel_id, data):
        self.oauth_data.update_one({"channel_id": channel_id}, {"$set": {"credentials": data}}, upsert=True)

    def get_oauth_credentials(self, channel_id):
        found = self.oauth_data.find_one({"channel_id": channel_id}, projection={"_id": False})
        if found:
            return found.get('credentials')

# if __name__ == '__main__':
#     st = YMStorage()
#     print st.get_exps_results_counts()
