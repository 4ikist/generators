import logging
import re

from googleapiclient.discovery import build
from stop_words import get_stop_words


from rr_lib.cm import ConfigManager
from wsgi.properties import YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION
from wsgi.youtube import next_page_iter, net_tryings, yt_date_to_tst


token_reg = re.compile("[\\W\\d]+")
stop_words_en = get_stop_words("en")


log = logging.getLogger("ym_engine")



class YoutubeManager(object):
    def __init__(self, yt=None):
        cm = ConfigManager()
        self.youtube = yt or build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                                   developerKey=cm.get('YOUTUBE_DEVELOPER_KEY'))

    def get_channel_info(self, channel_id, before_video_id=None, add_videos=True):
        q = {
            "part": "snippet",
            "channelId": channel_id,
            "maxResults": 50,
            "order": "date"
        }
        func = self.youtube.search().list
        result = {}
        videos = []
        before_found = False
        for item in next_page_iter(func, q):
            video_id = item.get("id").get("videoId")
            _channel_id = item.get("id").get("channelId")

            if _channel_id and _channel_id == channel_id:
                result['title'] = item['snippet']['title']
                result['description'] = item['snippet']['description']
                result['channel_id'] = _channel_id

            if video_id == before_video_id:
                before_found = True
            if (before_video_id and before_found) or not add_videos:
                if not result:
                    continue
                else:
                    break

            if video_id:
                videos.append({
                    "video_id": video_id,
                    "title": item['snippet']['title'],
                    "description": item['snippet']['description'],
                    "channel_title": item['snippet']['channelTitle'],
                    "channel_id": item['snippet']['channelId']
                })

        return result, videos

    @net_tryings
    def get_video_info(self, video_id):
        video_response = self.youtube.videos().list(
            id=video_id,
            part='snippet,statistics,contentDetails'
        ).execute()
        for video_result in video_response.get("items", []):
            snippet = video_result.get("snippet", {})
            statistic_data = video_result["statistics"]
            content_details = video_result["contentDetails"]
            if snippet and statistic_data:
                description = snippet.get("description")
                title = snippet.get("title")
                img = snippet.get("thumbnails", {}).get("medium", {}).get("url")
                channel_title = snippet.get("channelTitle")

                return {
                    "description": description,
                    "title": title,
                    "img": img,
                    "id": video_id,
                    "video_id": video_id,
                    "channel_title": channel_title,
                    "channel_id": snippet.get("channelId"),
                    "licensed": content_details.get("licensedContent"),
                    "yt_comments": int(statistic_data.get('commentCount', 0)),
                    "yt_likes": int(statistic_data.get('likeCount', 0)),
                    "yt_dislikes": int(statistic_data.get('dislikeCount', 0)),
                    "yt_views": int(statistic_data.get('viewCount', 0)),
                    "yt_favorites": int(statistic_data.get('favoriteCount', 0)),
                    "tags": snippet.get("tags"),
                    "time": yt_date_to_tst(snippet.get("publishedAt")),
                    "category_id": snippet.get("categoryId")
                }


if __name__ == '__main__':
    yt = YoutubeManager()
    # yt.get_channel_info('UC5nVgiv6elz5GnS6MTr1tVA', add_videos=False)
    print yt.get_video_info('4ii4Jr3uL8A')['time']