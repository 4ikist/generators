from wsgi.youtube.sniffer.store import SnifferConfigStorage

if __name__ == '__main__':
    ss = SnifferConfigStorage()
    for category_data in ss.categories.find({}):
        print category_data
        words = category_data.get("words")
        cat_name = category_data.get("name")
        lower_words = []
        for word in words:
            ss.words.update_one({"word": word}, {"$set": {"word": word.lower()}})
            lower_words.append(word.lower())

        ss.categories.update_one({"_id": category_data.get("_id")}, {"$set": {"words": lower_words}})

