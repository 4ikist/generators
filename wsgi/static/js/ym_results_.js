

function append_end_result(end_obj){
    console.log("END:",end_obj);
}

function append_error(error_obj){
    console.log("ERR:", error_obj)
}

function update_channel_info(ch_id, channel_info_obj){
    console.log("CH:",ch_id, channel_info_obj);
}


var main_timeout = null;
var exp_id = null;


function get_next(exp_id){
    $.ajax({
            type:"post",
            url:"/youtube/result"+exp_id,
            data: JSON.stringify(form_data),
            contentType:    'application/json',
            dataType:       'json',
            success: process_data
        });
}

function process_data(data){
                  if (data.ok){
                        if (data.end){
                            append_end_result(data.channel_ids.end);
                            if (main_timeout != null) {
                                clearTimeout(main_timeout);
                            }

                        } else if (data.error){
                            append_error(data.channel_ids.error);

                        } else {
                            var channel_ids = data.channel_ids;
                            for (var property in channel_ids) {
                                if (channel_ids.hasOwnProperty(property)) {
                                    update_channel_info(channel_ids[property]);
                                }
                            }
                            main_timeout = setTimeout(function(){
                               get_next(data.exp_id);
                            }, 2000);
                        }
                  }
}


function start_experiment(){
        var form_data = {};
        var list = $(".object").map(function(){
                return $(this).attr("level");
            }).get();

        $("#start-experiment").find(":input").each(function(){
            form_data[this.id] = $(this).val();
        });

        $.ajax({
            type:"post",
            url:"/youtube/start",
            data: JSON.stringify(form_data),
            contentType:    'application/json',
            dataType:       'json',
            success:    function(data){
                            if (data.ok){
                                    get_next(data.exp_id);
                            }
            }
        });
}



function init_charts(exp_id){
    $.ajax({
        type:"get",
        url:'/youtube/cache/'+exp_id,
        success:function(data){
            console.log(data);
            if (data.ok){
                var cached = data.cached;
                var channels_data = data.exp_data.load_data;

                $("#experiment-channels").empty();

                for (var result_id in cached) {
                    if (cached.hasOwnProperty(result_id)){
                        var result_obj = cached[result_id];
                         if (result_id == 'error'){
                            $("#error").text(result_obj.error_info);
                            continue;
                        }

                        if (result_obj != null && result_obj.tags != undefined){
                            show_channel_info(result_obj, exp_id, true);
                            draw_experiment_result(result_obj.tags, "Tags "+result_obj.title, result_id);
                        } else {
                            show_channel_info(result_obj, exp_id, false);
                        }
                    }
                }

            }
        }

    });
}

function show_channel_info(channel_data, exp_id, have_calc_data){
        channel_data['exp_id'] = exp_id;
        if (have_calc_data == true){
            channel_data['have_calc_data'] = true;
        }

        var result = Mustache.render(`
        <tr>
                <td>
                    <a href="https://www.youtube.com/channel/{{channel_id}}" target='_blank'>{{title}}</a>
                </td>
                <td>
                    {{videoCount}}
                </td>
                <td>
                    {{viewCount}}
                </td>
                <td>
                    {{subscriberCount}}
                </td>
                <td>
                    {{commentCount}}
                </td>


        {{#have_calc_data}}
                    <td class="exp-result">
                        <span style="opacity:0; display: none;">1</span>
                        <div class="main-{{channel_id}}">
                            <div class="accordion" id="info-accordion-{{channel_id}}">
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#info-accordion-{{channel_id}}"
                                           href="#{{channel_id}}">
                                            <span class="glyphicon glyphicon-barcode" aria-hidden="true"></span>

                                        </a>
                                    </div>
                                    <div id="{{channel_id}}" class="accordion-body collapse">
                                        <div class="accordion-inner col-md-12">
                                            <canvas id="canvas-{{channel_id}}"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
        {{/have_calc_data}}
        {{^have_calc_data}}
            <td>
               <span style="opacity:0; display: none;">0</span>
                <span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>
            </td>
        {{/have_calc_data}}
          <td>
               <button class="btn btn-sm btn-info" onclick="start_load_result('{{channel_id}}', '{{exp_id}}')">Start</button>
          </td>
        </tr>
        `, channel_data);

        $("#experiment-channels").append(result);
}

function start_load_result(channel_id, exp_id){
    $.ajax({
        type:"get",
        url:"/youtube/load_channel/"+channel_id+"/"+exp_id,
        success:function(data){
            if (data.ok){
                draw_experiment_result(data.tags, data.channel_info.title, channel_id);
            }
        }
    })
}


function draw_experiment_result(speeds, exp_info, result_id){
    console.log(result_id);
     var    labels = [],
            data = [],
            element = document.getElementById("canvas-"+result_id);

     if (element==null){
        $("#another-channels").append("<canvas id='canvas-"+result_id+"'></canvas>")
        element = document.getElementById("canvas-"+result_id);
     }

     var ctx = element.getContext("2d");
     speeds.sort(function(x,y){
        return x.v - y.v;
     });
     speeds.forEach(function(speed){
        labels.push(speed.k);
        data.push(speed.v);
     });

     var barChartData = {
            labels: labels,
            datasets: [{
                label: exp_info,
                backgroundColor: "rgb(220,220,220)",
                data: data,
            }
            ]
        };

     $("#canvas-"+result_id).parent(".exp-result").addClass("have-result");

     window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            elements: {
                rectangle: {
                    borderWidth: 2,
                    borderColor: 'rgb(100, 255, 200)',
                    borderSkipped: 'bottom'
                }
            },
            responsive: true,
            legend: {
                position: 'top',
            }
        }
    });
}