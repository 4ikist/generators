var chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(231,233,237)',
	black: 'rgb(255,255,255)',
	another: 'rgb(155,155,155)',
	another2: 'rgb(10,100,150)',
	another3: 'rgb(100,10,150)',
	another4: 'rgb(150,10,100)',
	another5: 'rgb(100,150,10)',
	another6: 'rgb(150,10,200)',
	another7: 'rgb(50,10,20)',
	another8: 'rgb(150,100,200)',
	another9: 'rgb(100,110,20)',
	another10: 'rgb(200,100,170)'
};
var colorNames = Object.keys(chartColors);
var color = Chart.helpers.color;



var barChartData = {
    labels:[],
    datasets:[]
};

var ch_labels = {};

function load_data(channel_id, exp_id){
    $("#load-"+channel_id).removeClass();
    $("#load-"+channel_id).addClass('at_load');
    $.ajax({
        url:"/youtube/load_channel/"+channel_id+"/"+exp_id,
        success:function(data){
            if (data.ok){
                add_calc_data(data.tags, data.channel_name);
            } else {
                $("#error").text(data.error);
            }
            $("#load-"+channel_id).removeClass();
            $("#load-"+channel_id).addClass('glyphicon glyphicon-barcode');
        }
    });
}
function push_to_datasets(){
    if (barChartData.datasets.length > 0){
        for(var i = 0; i < barChartData.datasets.length; i++){
            barChartData.datasets[i].data.push(0);
        }
    }
}
function add_calc_data(data, name){
    data.sort(function(x,y){
        return y.v - x.v;
    });
    data = data.slice(0,50);

    datasetData = new Array(Object.keys(ch_labels).length);
    datasetData.fill(0);

    data.forEach(function(el){
        if (el.k in ch_labels){
            var position = ch_labels[el.k];
            datasetData[position] = el.v;
        } else{
            var position = datasetData.push(el.v);
            ch_labels[el.k] = position - 1;
            push_to_datasets();
        }

        console.log(el.k, datasetData[ch_labels[el.k]], el.v);
    });
    
    var colorName = colorNames[barChartData.datasets.length % colorNames.length];;
    var dsColor = window.chartColors[colorName];
    
    var newDataset = {
                label: name,
                backgroundColor: color(dsColor).alpha(0.5).rgbString(),
                borderColor: dsColor,
                borderWidth: 1,
                data: datasetData
    };
    barChartData.datasets.push(newDataset);
    barChartData.labels = Object.keys(ch_labels);
    barChart.update();
}

window.onload = function() {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.barChart = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Tags of all loaded channels'
                    }
                }
            });

 };


